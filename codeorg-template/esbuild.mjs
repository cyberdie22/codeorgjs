import esbuild from 'esbuild';

esbuild.build({
    entryPoints: ['src/index.ts'],
    bundle: true,
    // minify: true,
    outfile: 'out/out.js',
    treeShaking: true,
})