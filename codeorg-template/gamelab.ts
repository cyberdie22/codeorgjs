// Game Lab
function drawSprites(): void {}
function playSound(url: string, loop: boolean | null): void {}
function stopSound(url: string): void {}
function playSpeech(text: string, gender: string, langauge: string | null): void {}
function keyDown(code: string): boolean { return false; }
function keyWentDown(code: string): boolean { return false; }
function keyWentUp(code: string): boolean { return false; }
function mouseDidMove(): boolean { return false; }
function mouseDown(button: string): boolean { return false; }
function mouseIsOver(sprite: Sprite): boolean { return false; }
function mouseWentDown(button: string): boolean { return false; }
function mouseWentUp(button: string): boolean { return false; }
function mousePressedOver(sprite: Sprite): boolean { return false; }
function showMobileControls(spaceButtonVisible: boolean, dpadVisible: boolean, dpadFourWay: boolean, mobileOnly: boolean): void {}

// Sprites
function createSprite(x: number, y: number, width?: number, height?: number): Sprite { return new Sprite(); }
function createEdgeSprites(): void {}

class Sprite {
    public x: number;
    public y: number;
    public width: number;
    public height: number;
    public debug: boolean;
    public depth: number;
    public lifetime: number;
    public bouncieness: number;
    public rotateToDirection: boolean;
    public rotation: number;
    public rotationSpeed: number;
    public scale: number;
    public shapeColor: string;
    public tint: string;
    public velocityX: number;
    public velocityY: number;
    public visible: boolean;

    public setSpeedAndDirection(speed: number, angle: number): void {}
    public getDirection(): number { return 0; }
    public getSpeed(): number { return 0; }
    public isTouching(target: Sprite | Group): boolean { return false; }
    public destroy(): void {}
    public pointTo(x: number, y: number): void {}
    public bounce(target: Sprite | Group): boolean { return false; }
    public bounceOff(target: Sprite | Group): boolean { return false; }
    public collide(target: Sprite | Group): boolean { return false; }
    public displace(target: Sprite | Group): boolean { return false; }
    public overlap(target: Sprite | Group): boolean { return false; }
    public setAnimation(label: string): void {}
    public mirrorX(dir?: number): number | null { return 0; }
    public mirrorY(dir?: number): number | null { return 0; }
    public nextFrame(): void {}
    public pause(): void {}
    public play(): void {}
    public setCollider(type: string, xOffset?: number, yOffset?: number, width?: number, height?: number, rotationOffset?: number): void {}
    public setFrame(frame: number): void {}
    public setVelocity(x: number, y: number): void {}
    public getScaledWidth(): number { return 0; }
    public getScaledHeight(): number { return 0; }
}

// Groups
function createGroup(): Group { return new Group() }

class Group {
    public add(sprite: Sprite): void {}
    public remove(sprite: Sprite): void {}
    public clear(): void {}
    public contains(sprite: Sprite): boolean { return false; }
    public get(i: number): Sprite { return new Sprite() }
    public isTouching(target: Sprite | Group): boolean { return false; }
    public bounce(target: Sprite | Group): boolean { return false; }
    public bounceOff(target: Sprite | Group): boolean { return false; }
    public collide(target: Sprite | Group): boolean { return false; }
    public displace(target: Sprite | Group): boolean { return false; }
    public overlap(target: Sprite | Group): boolean { return false; }
    public maxDepth(): number { return 0; }
    public minDepth(): number { return 0; }
    public destroyEach(): void {}
    public pointToEach(x: number, y: number): void {}
    public setAnimation(label: string): void {}
    public mirrorX(dir?: number): number | null { return 0; }
    public mirrorY(dir?: number): number | null { return 0; }
    public setWidth(width: number): void {}
    public setHeight(height: number): void {};
    public setDepth(depth: number): void {};
    public setLifetime(lifetime: number): void {};
    public setRotateToDirection(bool: boolean): void {};
    public setRotation(rotation: number): void {};
    public setRotationSpeed(rotationSpeed: number): void {};
    public setScale(scale: number): void {};
    public setShapeColor(shapeColor: string): void {};
    public setTint(tint: string): void {};
    public setVelocity(x: number, y: number): void {};
    public setVelocityX(velocity: number): void {};
    public setVelocityY(velocity: number): void {};
    public setVisible(visible: boolean): void {};
}

// Drawing
function background(color: string): void {}
function fill(color: string): void {}
function noFill(): void {}
function stroke(color: string): void {}
function strokeWeight(size: number): void {}
function rgb(r: number, g?: number, b?: number, a?: number): any {}
function noStroke(): void {}
function arc(x: number, y: number, w: number, h: number, start: number, stop: number): void {}
function ellipse(x: number, y: number, w: number, h: number): void {}
function line(x1: number, y1: number, x2: number, y2: number): void {}
function point(x: number, y: number): void {}
function rect(x: number, y: number, w: number, h: number): void {}
function regularPolygon(x: number, y: number, sides: number, size: number): void {}
function shape(...args: number[]): void {}
function text(str: string, x: number, y: number, width: number, height: number): void {}
function textAlign(horiz: "Left" | "Center" | "Right", vert?: "Top" | "Bottom" | "Center" | "Baseline"): void {}
function textFont(font: string): void {}
function textSize(pixels: number): void {}