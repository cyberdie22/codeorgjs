"use strict";

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
  // src/events.ts
  var StartDrawEvent = function StartDrawEvent() {
    _classCallCheck(this, StartDrawEvent);
  };

  var EndDrawEvent = function EndDrawEvent() {
    _classCallCheck(this, EndDrawEvent);
  };

  var MouseDownEvent = function MouseDownEvent(key) {
    _classCallCheck(this, MouseDownEvent);

    this.button = key;
  };

  var MouseHoldEvent = function MouseHoldEvent(key) {
    _classCallCheck(this, MouseHoldEvent);

    this.button = key;
  };

  var MouseReleaseEvent = function MouseReleaseEvent(key) {
    _classCallCheck(this, MouseReleaseEvent);

    this.button = key;
  };

  var KeyDownEvent = function KeyDownEvent(key) {
    _classCallCheck(this, KeyDownEvent);

    this.key = key;
  };

  var KeyHoldEvent = function KeyHoldEvent(key) {
    _classCallCheck(this, KeyHoldEvent);

    this.key = key;
  };

  var KeyReleaseEvent = function KeyReleaseEvent(key) {
    _classCallCheck(this, KeyReleaseEvent);

    this.key = key;
  };

  function checkKey(key) {
    if (keyWentDown(key)) EventHandler.dispatch("KeyDownEvent", new KeyDownEvent(key));
    if (keyDown(key)) EventHandler.dispatch("KeyHoldEvent", new KeyHoldEvent(key));
    if (keyWentUp(key)) EventHandler.dispatch("KeyReleaseEvent", new KeyReleaseEvent(key));
  }

  function checkButton(button) {
    if (mouseWentDown(button)) EventHandler.dispatch("KeyDownEvent", new MouseDownEvent(button));
    if (mouseDown(button)) EventHandler.dispatch("KeyHoldEvent", new MouseHoldEvent(button));
    if (mouseWentUp(button)) EventHandler.dispatch("KeyReleaseEvent", new MouseReleaseEvent(button));
  }

  var EventHandlerClass = /*#__PURE__*/function () {
    function EventHandlerClass() {
      _classCallCheck(this, EventHandlerClass);

      this.handlers = {};
    }

    _createClass(EventHandlerClass, [{
      key: "on",
      value: function on(eventName, handler) {
        var handlerArray = this.handlers[eventName];

        if (!handlerArray) {
          handlerArray = [];
        }

        handlerArray.push(handler);
        this.handlers[eventName] = handlerArray;
      }
    }, {
      key: "dispatch",
      value: function dispatch(eventName, event) {
        var eventHandlers = this.handlers[eventName];

        if (eventHandlers) {
          eventHandlers.forEach(function (handler) {
            return handler(event);
          });
        }
      }
    }]);

    return EventHandlerClass;
  }();

  var EventHandler = new EventHandlerClass(); // src/index.ts

  var keys = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "`", "~", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "=", "{", "[", "]", "}", "\\", "|", ":", ";", '"', "'", "<", ",", ">", ".", "?", "/", "tab", "esc", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", "insert", "home", "delete", "end", "up", "down", "left", "right"];
  var buttons = ["left", "right"];
  EventHandler.on("KeyDownEvent", function (event) {
    if (event.key === "w") {
      console.log("hello, world!");
    }
  });

  function draw() {
    EventHandler.dispatch("StartDrawEvent", new StartDrawEvent());
    keys.forEach(function (key) {
      return checkKey(key);
    });
    buttons.forEach(function (button) {
      return checkButton(button);
    });
    EventHandler.dispatch("EndDrawEvent", new EndDrawEvent());
  }

  draw();
  drawSprites();
})();
