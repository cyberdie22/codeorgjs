export interface Signal {}

export interface DrawEvent extends Signal {}

export class StartDrawEvent implements DrawEvent {}
export class EndDrawEvent implements DrawEvent {}

export interface MouseEvent extends Signal {
    button: string;
}
export interface KeyEvent extends Signal {
    key: string
}

export class MouseDownEvent implements MouseEvent {
    button: string

    constructor(key: string) {
        this.button = key
    }
}
export class MouseHoldEvent implements MouseEvent {
    button: string

    constructor(key: string) {
        this.button = key
    }
}
export class MouseReleaseEvent implements MouseEvent {
    button: string

    constructor(key: string) {
        this.button = key
    }
}

export class KeyDownEvent implements KeyEvent {
    key: string

    constructor(key: string) {
        this.key = key
    }
}
export class KeyHoldEvent implements KeyEvent {
    key: string

    constructor(key: string) {
        this.key = key
    }
}
export class KeyReleaseEvent implements KeyEvent {
    key: string

    constructor(key: string) {
        this.key = key
    }
}

export function checkKey(key: string): void {
    if (keyWentDown(key)) EventHandler.dispatch('KeyDownEvent', new KeyDownEvent(key))
    if (keyDown(key)) EventHandler.dispatch('KeyHoldEvent', new KeyHoldEvent(key))
    if (keyWentUp(key)) EventHandler.dispatch('KeyReleaseEvent', new KeyReleaseEvent(key))
}

export function checkButton(button: string): void {
    if (mouseWentDown(button)) EventHandler.dispatch('KeyDownEvent', new MouseDownEvent(button))
    if (mouseDown(button)) EventHandler.dispatch('KeyHoldEvent', new MouseHoldEvent(button))
    if (mouseWentUp(button)) EventHandler.dispatch('KeyReleaseEvent', new MouseReleaseEvent(button))
}

class EventHandlerClass {
    private handlers = {}

    on<T>(eventName: string, handler: (event: T) => void) {
        let handlerArray = this.handlers[eventName]
        if (!handlerArray) {
            handlerArray = []
        }
        handlerArray.push(handler)
        this.handlers[eventName] = handlerArray
    }

    dispatch(eventName: string, event: Signal) {
        let eventHandlers = this.handlers[eventName]
        if (eventHandlers) {
            eventHandlers.forEach(handler => handler(event))
        }
    }
}

export const EventHandler = new EventHandlerClass()