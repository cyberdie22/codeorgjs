import { KeyEvent, StartDrawEvent, EndDrawEvent, EventHandler, checkKey, checkButton } from './events'

const keys = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', 
'1', '2', '3', '4', '5', '6', '7', '8', '9', '`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+', '=', '{', '[', ']', '}', 
'\\', '|', ':', ';', '"', "'", '<', ',', '>', '.', '?', '/', 'tab', 'esc', 'f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', 'f10', 'f11', 
'f12', 'insert', 'home', 'delete', 'end', 'up', 'down', 'left', 'right']
const buttons = ['left', 'right']

EventHandler.on('KeyDownEvent', (event: KeyEvent) => {
    if (event.key === 'w') {
        console.log('hello, world!')
    }
})

function draw() {
    EventHandler.dispatch('StartDrawEvent', new StartDrawEvent())
    keys.forEach(key => checkKey(key))
    buttons.forEach(button => checkButton(button))
    EventHandler.dispatch('EndDrawEvent', new EndDrawEvent())
}
draw()
drawSprites()